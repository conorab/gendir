# gendir

An old script that would go through every GoPro video in a folder and generate a thumbnail and webpage with every video displayed and using the generated thumbnails. This script was made in 2017 and is no longer maintained.
